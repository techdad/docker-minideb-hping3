FROM bitnami/minideb:latest

LABEL description="hping3 running on bitnami/minideb"

RUN install_packages tini hping3

RUN ln -s /usr/share/doc/hping3/examples/hpingstdlib.htcl /hpingstdlib.htcl

ENTRYPOINT ["/usr/bin/tini", "-e", "1", "--", "hping3"]

