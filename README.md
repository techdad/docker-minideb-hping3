# Docker hping3 (in minideb)

A very simple Docker container to run `hping3`

[![pipeline status](https://gitlab.com/techdad/docker-minideb-hping3/badges/main/pipeline.svg)](https://gitlab.com/techdad/docker-minideb-hping3/-/commits/main)

+ Based on the `sflow/hping3` container...
+ But running on `bitnami/minideb` (instead of Alpine).
